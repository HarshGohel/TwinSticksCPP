// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Containers/Map.h" 
#include "GamemodeInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI , Blueprintable)
class UGamemodeInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TWINSTICKSCPP_API IGamemodeInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual void SetPlayerKills(float playerkills);
	virtual void SetAllyKills(float allykills);
	virtual void SetEnemyKills();
	virtual void SetEnemyRef(class AEnemyPawn *EnemyPawn);

	
	
	virtual void PawnHandleDeath(AActor *DeadActor);
	


	UFUNCTION(BlueprintNativeEvent , BlueprintCallable)
	TArray<float> HUDGetTimer();

	UFUNCTION(BlueprintNativeEvent , BlueprintCallable)
	TArray<float> HUDkills();



};
