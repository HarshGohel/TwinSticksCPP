// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "ShooterInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI , Blueprintable)
class UShooterInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TWINSTICKSCPP_API IShooterInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual void HandleDeath();
	virtual void IncreaseKill();
	virtual USceneComponent* GetSpawnActor();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Pawnfire();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	float GetHealth();


	//controller interface
	virtual void UnSetHostileSeen();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	AActor* GetHostilePawn();

	UFUNCTION(BlueprintImplementableEvent)
	void BPUnSetHostileSeen();

	virtual void SetCanMove(bool CanMove);




};
