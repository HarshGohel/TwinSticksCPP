// Fill out your copyright notice in the Description page of Project Settings.

#include "../Source/TwinSticksCPP/Interfaces/Public/ShooterInterface.h"

// Add default functionality here for any IShooterInterface functions that are not pure virtual.

void IShooterInterface::HandleDeath()
{
    GEngine->AddOnScreenDebugMessage(0, 1, FColor::Red, FString::Printf(TEXT("Default Handle Death Imterface")));
}

void IShooterInterface::IncreaseKill()
{
    GEngine->AddOnScreenDebugMessage(0, 1, FColor::Red, FString::Printf(TEXT("Default Increase kill Imterface")));
}

// void IShooterInterface::FireProjectile(USceneComponent *ProjectileSpawn)
// {
//     GEngine->AddOnScreenDebugMessage(0, 1, FColor::Red, FString::Printf(TEXT("Default Fire Imterface")));
// }


USceneComponent* IShooterInterface::GetSpawnActor()
{
    return nullptr;
}

void IShooterInterface::UnSetHostileSeen()
{
    UE_LOG(LogTemp, Warning, TEXT("Default unsethostileseen in shooterinterface"));
}

void IShooterInterface::SetCanMove(bool CanMove)
{
    UE_LOG(LogTemp, Warning, TEXT("Default SetCanmove in shooterinterface"));
}
