// Fill out your copyright notice in the Description page of Project Settings.

#include "../Source/TwinSticksCPP/Interfaces/Public/GamemodeInterface.h"
#include "../Source/TwinSticksCPP/Pawn/Public/EnemyPawn.h"

// Add default functionality here for any IGamemodeInterface functions that are not pure virtual.

void IGamemodeInterface::SetPlayerKills(float playerkills)
{
    UE_LOG(LogTemp, Warning, TEXT("Default set player kills"));
}

void IGamemodeInterface::SetAllyKills(float allykills)
{
    UE_LOG(LogTemp, Warning, TEXT("Default set Ally kills"));
}

void IGamemodeInterface::SetEnemyKills()
{
    UE_LOG(LogTemp, Warning, TEXT("Default set Enemy kills"));
}

void IGamemodeInterface::SetEnemyRef(AEnemyPawn *EnemyPawn)
{
    UE_LOG(LogTemp, Warning, TEXT("Default set Enemyref"));
}

void IGamemodeInterface::PawnHandleDeath(AActor *DeadActor)
{
    UE_LOG(LogTemp, Warning, TEXT("Default set handleDeath"));
}