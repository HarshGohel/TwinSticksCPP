// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DefaultPawn.h"
#include "../Source/TwinSticksCPP/Interfaces/Public/ShooterInterface.h"
#include "../Source/TwinSticksCPP/Interfaces/Public/GamemodeInterface.h"
#include "Components/WidgetComponent.h"
#include "BasePawn.generated.h"

/**
 *
 */

UCLASS()
class TWINSTICKSCPP_API ABasePawn : public ADefaultPawn, public IShooterInterface, public IGamemodeInterface
{
	GENERATED_BODY()

public:
	ABasePawn();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditDefaultsOnly, Category = Components)
	UStaticMeshComponent *PawnMesh;

	UPROPERTY(EditDefaultsOnly, Category = Components)
	USceneComponent *ProjectileSpawn;

	UPROPERTY(EditDefaultsOnly, Category = Components)
	class UHealthComponent *HealthComponent;

	UPROPERTY(Category = Components, EditDefaultsOnly)
	class USphereComponent *MyCollisionComponent;

	UPROPERTY(EditDefaultsOnly, Category = Components)
	class UProjectilePool *ProjectilePool;

public:
	// Interface Implementation
	virtual void HandleDeath() override;
	virtual USceneComponent *GetSpawnActor();

protected:
	virtual void Respawn(FVector RespawnLocation);

protected:
	bool CanFire = false;

	UPROPERTY(BlueprintReadOnly)
	bool isDead = false;

	// FX
	UPROPERTY(EditDefaultsOnly, Category = FX)
	class UParticleSystem *DeathParticle;
	UPROPERTY(EditDefaultsOnly, Category = FX)
	class USoundBase *DeathSound;

	// Interface variables
	public:
	IGamemodeInterface *GamemodeInterface;
	IShooterInterface *AIControllerInterface;


	// kills
	float kills = 0;

	// Interface
	public:
	void Pawnfire_Implementation();
	float GetHealth_Implementation();

};
