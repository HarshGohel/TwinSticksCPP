// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DefaultPawn.h"
#include "InputActionValue.h"
#include "../Source/TwinSticksCPP/Interfaces/Public/ShooterInterface.h"
#include "../Source/TwinSticksCPP/Interfaces/Public/GamemodeInterface.h"
#include "PlayerPawn.generated.h"

/**
 * 
 */
class USphereComponent;
class USpringArmComponent;
class UCameraComponent;
class UInputAction;
class UInputMappingContext;
class UGameplayStatics;
class UWorld;
class ACustomGameMode;


UCLASS()
class TWINSTICKSCPP_API APlayerPawn : public ADefaultPawn, public IShooterInterface , public IGamemodeInterface
{
	GENERATED_BODY()

	public:
	APlayerPawn();

	protected:
	virtual void BeginPlay() override;


	public:
	virtual void Tick( float DeltaTime ) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	//Components
	protected:
	UPROPERTY(EditDefaultsOnly , Category=Components)
	UStaticMeshComponent *PawnMesh;
	UPROPERTY(EditDefaultsOnly, Category=Components)
	USpringArmComponent *CameraBoom;
	UPROPERTY(EditDefaultsOnly, Category=Components)
	UCameraComponent *Camera;
	UPROPERTY(EditDefaultsOnly, Category=Components)
	USceneComponent *ProjectileSpawn;
	UPROPERTY(EditDefaultsOnly, Category=Components)
	class UProjectilePool *ProjectilePool;
	UPROPERTY(EditDefaultsOnly, Category=Components)
	class UHealthComponent *HealthComponent;



	//Input
	private:
	UPROPERTY(EditDefaultsOnly, Category = Input)
	UInputAction *InputActionFire;
	UPROPERTY(EditDefaultsOnly, Category = Input)
	UInputAction *InputActionMove;
	UPROPERTY(EditDefaultsOnly, Category = Input)
	UInputAction *InputActionShowKD;
	UPROPERTY(EditDefaultsOnly, Category = Input)
	UInputMappingContext *MappingContext;

	class USphereComponent *MyCollisionComponent;


	//SFX and VFX
	UPROPERTY(EditDefaultsOnly, Category = FX)
	class UParticleSystem *DeathParticle;
	UPROPERTY(EditDefaultsOnly, Category = FX)
	class USoundBase *DeathSound;




	void Move(const FInputActionValue &Value);
	void Fire(const FInputActionValue &Value);
	void StopFire(const FInputActionValue &Value);
	void StartFire();
	void RespawnPlayer();


	//General Variables
	private:
	APlayerController *PlayerController;
	FHitResult HitResult;
	float Kills=0;
	FTimerHandle PlayerRespawnTimer;

	protected:
	UPROPERTY(BlueprintReadOnly)	
	bool isDead=false;



	//General Functions
	void RotateActor();

	//Timer Fire
	FTimerHandle FireTimerHandle;


	public:
	//Shooter Interface Implementation
	virtual void HandleDeath() override;
	virtual void IncreaseKill() override;
	USceneComponent* GetSpawnActor() override;
	float GetHealth_Implementation();
	
	private:
	IGamemodeInterface *GamemodeInterface;



	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class APlayerStart> PlayerStart;

};
