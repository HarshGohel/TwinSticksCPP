// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePawn.h"
#include "AllyPawn.generated.h"

/**
 *
 */
class AAllySpawnPoint;

UCLASS()
class TWINSTICKSCPP_API AAllyPawn : public ABasePawn
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;


private:
	FTimerHandle RespawnTimer;
	FTimerDelegate TimerDelegate;


public:
	// Interface Implementation
	virtual void HandleDeath() override;
	virtual void IncreaseKill() override;

	UFUNCTION()
	virtual void Respawn(FVector RespawnLocation) override;


	// kills
private:
	float Kills = 0;
	class UProjectilePool *TempProjectilePool;
	FVector SpawnLocation;


	// general funciton
public:
	void SetSpawnLocation(FVector SpawnLoc);
};
