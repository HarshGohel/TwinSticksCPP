// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePawn.h"
#include "EnemyPawn.generated.h"

/**
 * 
 */

class AEnemySpawnPoint;

UCLASS()
class TWINSTICKSCPP_API AEnemyPawn : public ABasePawn
{
	GENERATED_BODY()

	protected:
	virtual void BeginPlay() override;
	
	private:
	FTimerHandle RespawnTimer;
	FTimerDelegate TimerDelegate;



	public:
	//Interface Implementation
	virtual void HandleDeath() override;
	virtual void IncreaseKill() override;


	private:
	UFUNCTION()
	virtual void Respawn(FVector RespawnLocation) override;

	//kills
	private:
	float kills=0;
	class UProjectilePool *TempProjectilePool;
	FVector SpawnLocation;


	// private:
	// IGamemodeInterface *GamemodeInterface;
	// IShooterInterface *AIControllerInterface;


	//get kills for game mode
	public:
	float GetKills();

	//general func
	public:
	void SetSpawnLocation(FVector SpawnLoc);

};
