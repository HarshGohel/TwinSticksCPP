// Fill out your copyright notice in the Description page of Project Settings.

#include "../Source/TwinSticksCPP/Pawn/Public/EnemyPawn.h"
#include "Kismet/GameplayStatics.h"
#include "../Source/TwinSticksCPP/Spawnpoints/Public/EnemySpawnPoint.h"


void AEnemyPawn::BeginPlay()
{
  Super::BeginPlay();
  // UE_LOG(LogTemp, Warning, TEXT("%s") , *this->GetName());
  TimerDelegate.BindUFunction(this , "Respawn",  SpawnLocation);
  
  if(GamemodeInterface)
  {
    GamemodeInterface->SetEnemyRef(this);
  }
}

void AEnemyPawn::HandleDeath()
{
  Super::HandleDeath();
  GetWorldTimerManager().SetTimer(RespawnTimer, TimerDelegate , 2.f, false);
}

void AEnemyPawn::Respawn(FVector RespawnLocation)
{
  // all functionality in base class
  Super::Respawn(SpawnLocation);
}

void AEnemyPawn::IncreaseKill()
{
  kills++;
  GamemodeInterface->SetEnemyKills();
  AIControllerInterface->UnSetHostileSeen();
}

float AEnemyPawn::GetKills()
{
  return kills;
}

void AEnemyPawn::SetSpawnLocation(FVector SpawnLoc)
{
    // UE_LOG(LogTemp, Warning, TEXT("Spawn location called"));
    SpawnLocation = SpawnLoc;
}