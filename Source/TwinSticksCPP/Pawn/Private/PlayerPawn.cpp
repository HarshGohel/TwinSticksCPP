// Fill out your copyright notice in the Description page of Project Settings.

#include "../Source/TwinSticksCPP/Pawn/Public/PlayerPawn.h"
#include "../Source/TwinSticksCPP/Utility/Public/ProjectilePool.h"
#include "../Source/TwinSticksCPP/Components/Public/HealthComponent.h"
#include "../Source/TwinSticksCPP/Utility/Public/CustomGameState.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/SphereComponent.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerStart.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystem.h" 
#include "Sound/SoundBase.h" 
#include "Kismet/KismetMathLibrary.h"

APlayerPawn::APlayerPawn()
{
    PrimaryActorTick.bCanEverTick = true;

    PawnMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cone"));
    PawnMesh->SetupAttachment(RootComponent);

    ProjectileSpawn = CreateDefaultSubobject<USceneComponent>(TEXT("ProjectileSpawn"));
    ProjectileSpawn->SetupAttachment(PawnMesh);

    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
    CameraBoom->SetupAttachment(PawnMesh);

    Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
    Camera->SetupAttachment(CameraBoom);

    ProjectilePool = CreateDefaultSubobject<UProjectilePool>(TEXT("ProjectilePool"));
    this->AddOwnedComponent(ProjectilePool);

    HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
    this->AddOwnedComponent(HealthComponent);

    MyCollisionComponent = this->FindComponentByClass<USphereComponent>();


    ProjectileSpawn->SetRelativeLocation(FVector(0, 0, 60));
    ProjectileSpawn->SetRelativeRotation(FRotator(90, 0, 0));

    CameraBoom->bInheritPitch = false;
    CameraBoom->bInheritRoll = false;
    CameraBoom->bInheritYaw = false;
    CameraBoom->TargetArmLength = 1500;
    CameraBoom->SetRelativeRotation(FRotator(-90, -90, 0));

    PawnMesh->SetRelativeLocation(FVector(20, 0, 0));
    PawnMesh->SetRelativeRotation(FRotator(-90, 0, 0));

    Camera->SetRelativeRotation(FRotator(0, 0, 90));
}

void APlayerPawn::BeginPlay()
{
    Super::BeginPlay();
    PlayerController = Cast<APlayerController>(GetController());
    if (PlayerController)
    {
        if (UEnhancedInputLocalPlayerSubsystem *Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
        {
            Subsystem->AddMappingContext(MappingContext, 0);
        }
    }

    GamemodeInterface = Cast<IGamemodeInterface>(Cast<ACustomGameState>(UGameplayStatics::GetGameState(this)));


    // if(GamemodeInterface )
    // {
    //     UE_LOG(LogTemp, Warning, TEXT("yes"));
    // }
    // else
    // {
    //     UE_LOG(LogTemp, Warning, TEXT("no"));
    // }
}

void APlayerPawn::Tick(float DeltaTime)
{
    if (PlayerController)
    {
        PlayerController->GetHitResultUnderCursor(ECollisionChannel::ECC_GameTraceChannel1, false, HitResult);
        RotateActor();
        // DrawDebugSphere(GetWorld(), HitResult.ImpactPoint, 25, 12, FColor::Red, false , -1.f);
    }
}

void APlayerPawn::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    if (UEnhancedInputComponent *Input = Cast<UEnhancedInputComponent>(PlayerInputComponent))
    {
        Input->BindAction(InputActionFire, ETriggerEvent::Started, this, &APlayerPawn::Fire);
        Input->BindAction(InputActionFire, ETriggerEvent::Completed, this, &APlayerPawn::StopFire);
        Input->BindAction(InputActionFire, ETriggerEvent::Canceled, this, &APlayerPawn::StopFire);
        Input->BindAction(InputActionMove, ETriggerEvent::Triggered, this, &APlayerPawn::Move);
    }
}

void APlayerPawn::Move(const FInputActionValue &Value)
{
    FVector2D Movement = Value.Get<FVector2D>();

    AddMovementInput(FVector(1, 0, 0), Movement.X);
    AddMovementInput(FVector(0, 1, 0), Movement.Y);
}

void APlayerPawn::Fire(const FInputActionValue &Value)
{
    
    
    // UE_LOG(LogTemp, Warning, TEXT("Firing"));

    GetWorldTimerManager().SetTimer(FireTimerHandle, this , &APlayerPawn::StartFire , 0.1 , true);
}

void APlayerPawn::StartFire()
{
    ProjectilePool->SpawnPooledObject();
}

void APlayerPawn::StopFire(const FInputActionValue &Value)
{
    // UE_LOG(LogTemp, Warning, TEXT("Stopped fire"));
    GetWorldTimerManager().ClearTimer(FireTimerHandle);
}

void APlayerPawn::RotateActor()
{
    FVector Direction = HitResult.ImpactPoint - GetActorLocation();
    FRotator Rotate = FRotator(-90, Direction.Rotation().Yaw, 0);

    PawnMesh->SetRelativeRotation(Rotate);
}

void APlayerPawn::IncreaseKill()
{
    // UE_LOG(LogTemp, Warning, TEXT("Player kills"));
    Kills++;
    if (GamemodeInterface)
    {
        GamemodeInterface->SetPlayerKills(Kills);
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("invalid gamestate Interface in playerpawn.cpp"));
    }

}

USceneComponent *APlayerPawn::GetSpawnActor()
{
    return ProjectileSpawn;
}

void APlayerPawn::HandleDeath()
{
    isDead = true;

    DisableInput(PlayerController);

    // UGameplayStatics::SpawnSoundAtLocation(GetWorld() ,DeathSound , GetActorLocation());
    // UGameplayStatics::SpawnEmitterAtLocation(GetWorld() , DeathParticle , GetActorLocation() , FRotator::ZeroRotator);
    MyCollisionComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    PawnMesh->ToggleVisibility(false);
    HealthComponent->ResetHealth();
    GetWorldTimerManager().SetTimer(PlayerRespawnTimer, this, &APlayerPawn::RespawnPlayer, 2, false);
}

void APlayerPawn::RespawnPlayer()
{
    if (PlayerStart)
    {
        isDead = false;
        // UE_LOG(LogTemp, Warning, TEXT("%s"), *UGameplayStatics::GetActorOfClass(GetWorld() , PlayerStart)->GetActorNameOrLabel());
        SetActorLocation(UGameplayStatics::GetActorOfClass(GetWorld(), PlayerStart)->GetActorLocation());
        EnableInput(PlayerController);
        MyCollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
        PawnMesh->ToggleVisibility(true);
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("playerstart cast failed"));
    }
}

float APlayerPawn::GetHealth_Implementation()
{
    return HealthComponent->GetHealth();
}
