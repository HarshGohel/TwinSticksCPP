// Fill out your copyright notice in the Description page of Project Settings.

#include "../Source/TwinSticksCPP/Pawn/Public/BasePawn.h"
#include "../Source/TwinSticksCPP/Components/Public/HealthComponent.h"
#include "../Source/TwinSticksCPP/Utility/Public/ProjectilePool.h"
#include "../Source/TwinSticksCPP/Utility/Public/CustomGameState.h"
#include "../Source/TwinSticksCPP/Gamemode/Public/CustomGameMode.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystem.h"
#include "Sound/SoundBase.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SphereComponent.h"

ABasePawn::ABasePawn()
{
    PrimaryActorTick.bCanEverTick = true;

    PawnMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cone"));
    PawnMesh->SetupAttachment(RootComponent);

    ProjectileSpawn = CreateDefaultSubobject<USceneComponent>(TEXT("ProjectileSpawn"));
    ProjectileSpawn->SetupAttachment(PawnMesh);

    HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComp"));
    // this->AddOwnedComponent(HealthComponent);
    this->AddOwnedComponent(HealthComponent);

    MyCollisionComponent = this->FindComponentByClass<USphereComponent>();

    ProjectilePool = CreateDefaultSubobject<UProjectilePool>(TEXT("ProjectilePool"));
    this->AddOwnedComponent(ProjectilePool);

    PawnMesh->SetRelativeLocation(FVector(20, 0, 0));
    PawnMesh->SetRelativeRotation(FRotator(-90, 0, 0));

    ProjectileSpawn->SetRelativeLocation(FVector(0, 0, 60));
    ProjectileSpawn->SetRelativeRotation(FRotator(90, 0, 0));
}

void ABasePawn::BeginPlay()
{
    Super::BeginPlay();
    isDead = false;
    CanFire = true;
    AddActorWorldOffset(FVector(0, 0, 91));
    // UE_LOG(LogTemp, Warning, TEXT("%s") , *MyCollisionComponent->GetName());

    // GamemodeInterface = Cast<IGamemodeInterface>(Cast<ACustomGameMode>(UGameplayStatics::GetGameMode(this)));
    GamemodeInterface = Cast<IGamemodeInterface>(Cast<ACustomGameState>(UGameplayStatics::GetGameState(this)));
    
    AIControllerInterface = Cast<IShooterInterface>(GetController());

    AIControllerInterface->SetCanMove(true);

    
}

void ABasePawn::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (GetVelocity() != FVector(0, 0, 0))
    {
        SetActorRotation(UKismetMathLibrary::MakeRotFromX(GetVelocity()));
    }
}

void ABasePawn::HandleDeath()
{
    AIControllerInterface->SetCanMove(false);
    CanFire = false;
    isDead = true;
    // UE_LOG(LogTemp, Warning, TEXT("Baseclass HandleDeath called"));
    MyCollisionComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    PawnMesh->ToggleVisibility(false);
}

void ABasePawn::Respawn(FVector RespawnLocation)
{
    // UE_LOG(LogTemp, Warning, TEXT("Baseclass respawn called"));
    isDead = false;
    HealthComponent->ResetHealth();
    AIControllerInterface->SetCanMove(true);
    SetActorLocation(RespawnLocation);
    AddActorWorldOffset(FVector(0, 0, 91));
    MyCollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    PawnMesh->ToggleVisibility(true);
    CanFire = true;
}

void ABasePawn::Pawnfire_Implementation()
{
    if(CanFire == true)
    ProjectilePool->SpawnPooledObject();
}

float ABasePawn::GetHealth_Implementation()
{
    return HealthComponent->GetHealth();
}

USceneComponent *ABasePawn::GetSpawnActor()
{
    return ProjectileSpawn;
}