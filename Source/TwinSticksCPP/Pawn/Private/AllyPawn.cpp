// Fill out your copyright notice in the Description page of Project Settings.

#include "../Source/TwinSticksCPP/Pawn/Public/AllyPawn.h"
#include "../Source/TwinSticksCPP/Gamemode/Public/CustomGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "../Source/TwinSticksCPP/Utility/Public/ProjectilePool.h"
#include "../Source/TwinSticksCPP/Spawnpoints/Public/AllySpawnPoint.h"

void AAllyPawn::BeginPlay()
{
    Super::BeginPlay();
    TimerDelegate.BindUFunction(this , "Respawn",  SpawnLocation);
}

void AAllyPawn::HandleDeath()
{
    Super::HandleDeath();
    GetWorldTimerManager().SetTimer(RespawnTimer, TimerDelegate, 2.f, false);
}

void AAllyPawn::Respawn(FVector RespawnLocation)
{
    Super::Respawn(SpawnLocation);
}

void AAllyPawn::IncreaseKill()
{
    Kills++;
    GamemodeInterface->SetAllyKills(Kills);
    AIControllerInterface->UnSetHostileSeen();
}

void AAllyPawn::SetSpawnLocation(FVector SpawnLoc)
{
    // UE_LOG(LogTemp, Warning, TEXT("Spawn location called"));
    SpawnLocation = SpawnLoc;
}