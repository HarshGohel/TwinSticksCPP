// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "CPPTaskFire.generated.h"

/**
 * 
 */
UCLASS()
class TWINSTICKSCPP_API UCPPTaskFire : public UBTTaskNode
{
	GENERATED_BODY()

	EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent &OwnerComp , uint8 *NodeMemory);

};
