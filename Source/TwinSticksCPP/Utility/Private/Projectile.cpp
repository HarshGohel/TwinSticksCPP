// Fill out your copyright notice in the Description page of Project Settings.


#include "../Source/TwinSticksCPP/Utility/Public/Projectile.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h" 
#include "GameFramework/ProjectileMovementComponent.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile"));
	RootComponent = ProjectileMesh;

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));

	ProjectileMovementComponent->InitialSpeed = 1500;
	ProjectileMovementComponent->MaxSpeed = 1500;
	ProjectileMovementComponent->ProjectileGravityScale = 0;
	ProjectileMovementComponent->Velocity = FVector(0,0,0);
	ProjectileMovementComponent->ProjectileGravityScale = 0;

}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();

	ProjectileMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ProjectileMesh->ToggleVisibility(false);
	ProjectileMesh->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::OnBeginOverlap);
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectile::ResetProjectile()
{
	ProjectileMesh->ToggleVisibility(false);
	// UE_LOG(LogTemp, Warning, TEXT("Reseting pos to %s") , *GetOwner()->GetActorLocation().ToString());
	// UE_LOG(LogTemp, Warning, TEXT("Resetting"));
	ProjectileMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ProjectileMovementComponent->Velocity= FVector::ZeroVector;
	SetActorLocation(GetOwner()->GetActorLocation());
	AttachToActor(GetOwner(), FAttachmentTransformRules::KeepWorldTransform);
}

void AProjectile::ActivateProjectile(FVector NewVelocity , FVector SpawnLocation,  FRotator SpawnRotation)
{
	// UE_LOG(LogTemp, Warning, TEXT("Setting Pos to %s"), *SpawnLocation.ToString());

	UGameplayStatics::PlaySoundAtLocation(GetWorld(), FireSound , GetActorLocation(), GetActorRotation());
	DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	SetActorLocation(SpawnLocation);
	SetActorRotation(SpawnRotation);
	ProjectileMesh->ToggleVisibility(true);
	ProjectileMovementComponent->Velocity = NewVelocity;
	ProjectileMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	GetWorldTimerManager().SetTimer(ResetProjectileHandle ,this, &AProjectile::ResetProjectile, 0.25 ,false );
}

void AProjectile::OnBeginOverlap(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	// GEngine->AddOnScreenDebugMessage(0, 1, FColor::Red, FString::Printf(TEXT("Overlapping %s"),*OtherActor->GetName()));

	if(OtherActor != GetOwner())
	{
		// UE_LOG(LogTemp, Warning, TEXT("Applying damage to %s"), *OtherActor->GetName());
		UGameplayStatics::ApplyDamage(OtherActor , Damage , GetOwner()->GetInstigatorController(), GetOwner()
		 , UDamageType::StaticClass());
		// ResetProjectile();
		// ResetProjectileHandle.Invalidate();
	}
	
}
