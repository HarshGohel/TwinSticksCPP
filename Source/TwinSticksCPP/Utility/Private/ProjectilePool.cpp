// Fill out your copyright notice in the Description page of Project Settings.

#include "../Source/TwinSticksCPP/Utility/Public/ProjectilePool.h"
#include "Engine/World.h"


// Sets default values for this component's properties
UProjectilePool::UProjectilePool()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UProjectilePool::BeginPlay()
{
	Super::BeginPlay();

	FVector SpawnLocation = GetOwner()->GetActorLocation();
	FRotator SpawnRotation = FRotator::ZeroRotator;

	UWorld *const World = GetWorld();
	if (PoolObject != nullptr)
	{
		if (World != nullptr)
		{

			for (int i = 0; i < TotalObjectsToSpawn; i++)
			{
				// UE_LOG(LogTemp, Warning, TEXT("PoolComponent BeginPlay"));
				PooledObjects.Add(World->SpawnActor<AProjectile>(PoolObject, SpawnLocation, SpawnRotation));
				PooledObjects[i]->AttachToActor(GetOwner(), FAttachmentTransformRules::KeepWorldTransform);
				PooledObjects[i]->SetOwner(GetOwner());
			}
		}
	}


	ShooterInterface = Cast<IShooterInterface>(GetOwner());
	if(ShooterInterface)
	{
		ProjectileSpawn = ShooterInterface->GetSpawnActor();
	}
	else
	{
	UE_LOG(LogTemp, Warning, TEXT("ShooterInterface fail"));

	}
}


// Called every frame
void UProjectilePool::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UProjectilePool::SpawnPooledObject()
{

	if (SpawnedObjectIndex < TotalObjectsToSpawn)
	{
		if (PooledObjects[SpawnedObjectIndex] != nullptr)
		{

			// GEngine->AddOnScreenDebugMessage(0, 0.5, FColor::Red, FString::Printf(TEXT("%d"), SpawnedObjectIndex));
			
			PooledObjects[SpawnedObjectIndex]->ActivateProjectile(ProjectileSpawn->GetForwardVector() * ProjectileVelocity, ProjectileSpawn->GetComponentLocation() , ProjectileSpawn->GetComponentRotation() );
			SpawnedObjectIndex++;
		}
	}
	else
	{
		SpawnedObjectIndex = 0;
		PooledObjects[SpawnedObjectIndex]->ActivateProjectile(ProjectileSpawn->GetForwardVector() * ProjectileVelocity, ProjectileSpawn->GetComponentLocation() , ProjectileSpawn->GetComponentRotation() );
		SpawnedObjectIndex++;
	}
}

void UProjectilePool::FireProjectile_Implementation()
{
	// UE_LOG(LogTemp, Warning, TEXT("Fired"));
	if (SpawnedObjectIndex < TotalObjectsToSpawn)
	{
		if (PooledObjects[SpawnedObjectIndex] != nullptr)
		{

			// GEngine->AddOnScreenDebugMessage(0, 0.5, FColor::Red, FString::Printf(TEXT("%d"), SpawnedObjectIndex));
			
			PooledObjects[SpawnedObjectIndex]->ActivateProjectile(ProjectileSpawn->GetForwardVector() * ProjectileVelocity, ProjectileSpawn->GetComponentLocation() , ProjectileSpawn->GetComponentRotation() );
			SpawnedObjectIndex++;
		}
	}
	else
	{
		SpawnedObjectIndex = 0;
		PooledObjects[SpawnedObjectIndex]->ActivateProjectile(ProjectileSpawn->GetForwardVector() * ProjectileVelocity, ProjectileSpawn->GetComponentLocation() , ProjectileSpawn->GetComponentRotation() );
		SpawnedObjectIndex++;
	}
}
