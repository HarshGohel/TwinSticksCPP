// Copyright Epic Games, Inc. All Rights Reserved.

#include "../Source/TwinSticksCPP/Utility/Public/TwinSticksCPP.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TwinSticksCPP, "TwinSticksCPP" );
