// Fill out your copyright notice in the Description page of Project Settings.


#include "../Source/TwinSticksCPP/Utility/Public/CustomGameState.h"
#include "../Source/TwinSticksCPP/Gamemode/Public/CustomGameMode.h"



void ACustomGameState::BeginPlay()
{
    Super::BeginPlay();
}

TArray<float> ACustomGameState::HUDkills_Implementation()
{
    return kills;
}

void ACustomGameState::SetPlayerKills(float playerkills)
{
    // UE_LOG(LogTemp, Warning, TEXT("%f"), playerkills);
    // PlayerKills = playerkills;
    kills[0] = playerkills;
    TeamAKills++;
}

void ACustomGameState::SetAllyKills(float allykills)
{
    // UE_LOG(LogTemp, Warning, TEXT("Allykills"));
    // AllyKill = allykills;
    kills[1] = allykills;
    TeamAKills++;
}

void ACustomGameState::SetEnemyKills()
{
    // UE_LOG(LogTemp, Warning, TEXT("Enemykills"));
    if (Enemy1 != nullptr && Enemy2 != nullptr)
    {
        kills[2] = Enemy1->GetKills();
        kills[3] = Enemy2->GetKills();

        TeamBKills = kills[2] + kills[3];
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("Enemy ref invalid in gamestate"));
    }
}

void ACustomGameState::SetEnemyRef(AEnemyPawn *EnemyPawn)
{
    if (istrue)
    {

        Enemy1 = EnemyPawn;
        // UE_LOG(LogTemp, Warning, TEXT("%s"), *Enemy1->GetName());
        istrue = false;
    }
    else
    {
        Enemy2 = EnemyPawn;
        // UE_LOG(LogTemp, Warning, TEXT("%s"), *Enemy2->GetName());
    }
}

int ACustomGameState::GetWinningTeam()
{
    if(TeamAKills > TeamBKills)
    {
        return 0;
    }
    else if(TeamBKills > TeamAKills)
    {
        return 1;
    }
    else
    {
        return 2;
    }
}