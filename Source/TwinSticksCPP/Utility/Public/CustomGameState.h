// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "../Source/TwinSticksCPP/Interfaces/Public/GamemodeInterface.h"
#include "../Source/TwinSticksCPP/Pawn/Public/EnemyPawn.h"

#include "CustomGameState.generated.h"

/**
 *
 */

class AEnemyPawn;

UCLASS()
class TWINSTICKSCPP_API ACustomGameState : public AGameState, public IGamemodeInterface
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;


	public:
	TArray<float> HUDkills_Implementation();

private:
	TArray<float> kills = {0,0,0,0};

	protected:
	// kill tracking
	UPROPERTY(BlueprintReadOnly)
	float TeamAKills = 0;
	UPROPERTY(BlueprintReadOnly)
	float TeamBKills = 0;



	// interface declaration
public:
	void SetPlayerKills(float playerkills) override;
	void SetAllyKills(float allykills) override;
	void SetEnemyKills() override;
	void SetEnemyRef(AEnemyPawn *EnemyPawn) override;

private:
	// enemy refs
	AEnemyPawn *Enemy1;
	AEnemyPawn *Enemy2;


	bool istrue = true;

public:
	UFUNCTION()
	int GetWinningTeam();
};
