// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Projectile.h"
#include "../Source/TwinSticksCPP/Interfaces/Public/ShooterInterface.h"
#include "ProjectilePool.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TWINSTICKSCPP_API UProjectilePool : public UActorComponent , public IShooterInterface
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UProjectilePool();


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


private:

	UPROPERTY(EditDefaultsOnly)
	int32 TotalObjectsToSpawn;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class AProjectile> PoolObject;
	protected:
	TArray<class AProjectile*> PooledObjects;

	int32 SpawnedObjectIndex=0;

	UPROPERTY(EditDefaultsOnly)
	float ProjectileVelocity=4000;

	USceneComponent *ProjectileSpawn;



public:
	TArray<class AProjectile*> GetPooledObjects();


public:
	void SpawnPooledObject();


	//shooter interface implementation
	public:
	void FireProjectile_Implementation();


	private:

	IShooterInterface *ShooterInterface;
};
