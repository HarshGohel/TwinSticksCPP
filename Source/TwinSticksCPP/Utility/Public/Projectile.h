// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"



class UProjectileMovementComponent;



UCLASS()
class TWINSTICKSCPP_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;



	protected:
	UPROPERTY(EditDefaultsOnly , Category = Components)
	UStaticMeshComponent *ProjectileMesh;

	UPROPERTY(EditDefaultsOnly , Category = Components)
	UProjectileMovementComponent *ProjectileMovementComponent;

	UPROPERTY(EditDefaultsOnly , Category = Sound)
	class USoundBase *FireSound;


// projectile activate and deactivate
public:

	void ActivateProjectile(FVector NewVelocity , FVector SpawnLocation, FRotator SpawnRotation);
	
	
	void ResetProjectile();





//Reset timer
FTimerHandle ResetProjectileHandle;

//Apply Damage Function
UFUNCTION()
void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
  UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);



private:
UPROPERTY(EditDefaultsOnly, Category=Damage)
float Damage=10;

};
