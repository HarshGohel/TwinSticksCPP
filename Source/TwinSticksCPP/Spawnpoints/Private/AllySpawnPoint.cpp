// Fill out your copyright notice in the Description page of Project Settings.


#include "../Source/TwinSticksCPP/Spawnpoints/Public/AllySpawnPoint.h"
#include "../Source/TwinSticksCPP/Pawn/Public/AllyPawn.h"

// Sets default values
AAllySpawnPoint::AAllySpawnPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAllySpawnPoint::BeginPlay()
{
	Super::BeginPlay();
	SpawnAlly();
}

// Called every frame
void AAllySpawnPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AAllySpawnPoint::SpawnAlly()
{
	if(IsValid(AllyPawnToSpawn))
	{
		SpawnedAlly = GetWorld()->SpawnActor<AAllyPawn>(AllyPawnToSpawn , GetActorLocation() , GetActorRotation());
		SpawnedAlly->SetSpawnLocation(GetActorLocation());
		// SpawnedAlly->AddActorWorldOffset(FVector(0, 0, 91));
		
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(0, 2, FColor::Red, FString::Printf(TEXT("AllyToSpawn is not valid in AllySpawnpoint.cpp")));
	}
}
