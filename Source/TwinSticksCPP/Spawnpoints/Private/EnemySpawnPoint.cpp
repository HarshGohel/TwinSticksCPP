// Fill out your copyright notice in the Description page of Project Settings.

#include "../Source/TwinSticksCPP/Spawnpoints/Public/EnemySpawnPoint.h"
#include "../Source/TwinSticksCPP/Pawn/Public/EnemyPawn.h"


// Sets default values
AEnemySpawnPoint::AEnemySpawnPoint()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AEnemySpawnPoint::BeginPlay()
{
	Super::BeginPlay();
	SpawnEnemy();

	// UE_LOG(LogTemp, Warning, TEXT("%s") , *SpawnedEnemy->GetName());
}

// Called every frame
void AEnemySpawnPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AEnemySpawnPoint::SpawnEnemy()
{
	if (IsValid(EnemyPawnToSpawn))
	{
		SpawnedEnemy = GetWorld()->SpawnActor<AEnemyPawn>(EnemyPawnToSpawn, GetActorLocation(), GetActorRotation());
		// SpawnedEnemy->AddActorWorldOffset(FVector(0, 0, 91));
		SpawnedEnemy->SetSpawnLocation(GetActorLocation());
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(0, 2, FColor::Red, FString::Printf(TEXT("EnemyToSpawn is not valid in EnemySpawnpoint.cpp")));
		
	}
}
