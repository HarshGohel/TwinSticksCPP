// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AllySpawnPoint.generated.h"

UCLASS()
class TWINSTICKSCPP_API AAllySpawnPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAllySpawnPoint();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	public:
	void SpawnAlly();


private:

	UPROPERTY(EditDefaultsOnly , Category = Spawn)
	TSubclassOf<class AAllyPawn> AllyPawnToSpawn;

	class AAllyPawn *SpawnedAlly;


};
