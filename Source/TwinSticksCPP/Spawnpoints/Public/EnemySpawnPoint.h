// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemySpawnPoint.generated.h"

class AEnemyPawn;

UCLASS()
class TWINSTICKSCPP_API AEnemySpawnPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemySpawnPoint();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


public:
	void SpawnEnemy();


private:

	UPROPERTY(EditDefaultsOnly , Category = Spawn)
	TSubclassOf<AEnemyPawn> EnemyPawnToSpawn;

	AEnemyPawn *SpawnedEnemy;

};
