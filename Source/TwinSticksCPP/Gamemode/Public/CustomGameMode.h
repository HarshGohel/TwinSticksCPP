// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "../Source/TwinSticksCPP/Interfaces/Public/GamemodeInterface.h"
#include "Kismet/GameplayStatics.h"
#include "CustomGameMode.generated.h"

/**
 *
 */
class AEnemyPawn;


UCLASS()
class TWINSTICKSCPP_API ACustomGameMode : public AGameMode, public IGamemodeInterface
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;



	// TimerHandle
private:
	FTimerHandle GameTimer;
	FTimerHandle RestartDelay;

private:
	void UpdateGameTimer();

public:
	UFUNCTION(BlueprintImplementableEvent)
	void EndGame(int Team);

	UPROPERTY(EditDefaultsOnly, Category = Gametimer)
	float Second = 30;
	UPROPERTY(EditDefaultsOnly, Category = Gametimer)
	float Minute = 1;


	TArray<float> Timer = {Minute,Second};


	private:
	class ACustomGameState *GameState;



	//Interface get 
	public:
	TArray<float> HUDGetTimer_Implementation();


	//Pawn respawn and handle death interface
	virtual void PawnHandleDeath(AActor *DeadActor) override;


	UPROPERTY(EditDefaultsOnly, Category = FX)
	class UParticleSystem *DeathParticle;
	UPROPERTY(EditDefaultsOnly, Category = FX)
	class USoundBase *DeathSound;

};
