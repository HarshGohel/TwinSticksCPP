// Fill out your copyright notice in the Description page of Project Settings.

#include "../Source/TwinSticksCPP/Gamemode/Public/CustomGameMode.h"
#include "../Source/TwinSticksCPP/Utility/Public/CustomGameState.h"
#include "../Source/TwinSticksCPP/Pawn/Public/EnemyPawn.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Sound/SoundBase.h"

void ACustomGameMode::BeginPlay()
{
    Super::BeginPlay();
    GameState = Cast<ACustomGameState>(UGameplayStatics::GetGameState(this));
    GetWorldTimerManager().SetTimer(GameTimer, this, &ACustomGameMode::UpdateGameTimer, 1, true);
}

void ACustomGameMode::UpdateGameTimer()
{
    // timer[0] is minute
    // timer[1] is second

    if (Timer[1] > 0)
    {
        Timer[1]--;
    }
    else
    {
        if (Timer[0] > 0)
        {
            Timer[0]--;
            Timer[1] = 60;
        }
        else
        {
            GameTimer.Invalidate();
            EndGame(GameState->GetWinningTeam());
        }
    }
}

TArray<float> ACustomGameMode::HUDGetTimer_Implementation()
{
    return Timer;
}

// couldn't migrate all of the code into gamemode
void ACustomGameMode::PawnHandleDeath(AActor *DeadActor)
{
    // UE_LOG(LogTemp, Warning, TEXT("Pawnhandle called"));

    UGameplayStatics::SpawnSoundAtLocation(GetWorld(), DeathSound, DeadActor->GetActorLocation());
    UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DeathParticle, DeadActor->GetActorLocation(), FRotator::ZeroRotator);
}