// Fill out your copyright notice in the Description page of Project Settings.

#include "../Source/TwinSticksCPP/Controllers/Public/AllyTeamAIController.h"
#include "BehaviorTree/BlackboardComponent.h"

AAllyTeamAIController::AAllyTeamAIController()
{
    SetGenericTeamId(FGenericTeamId(0));

    AIPerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception"));
    this->AddOwnedComponent(AIPerceptionComponent);

    AllyBBD = CreateDefaultSubobject<UBlackboardComponent>(TEXT("blackboard"));
    this->AddOwnedComponent(AIPerceptionComponent);
}

void AAllyTeamAIController::BeginPlay()
{
    Super::BeginPlay();

    HasSeenHostile = false;
    // AIPerceptionComponent->OnPerceptionUpdated.AddDynamic(this, &AEnemyTeamAIController::OnPlayerDetected);
    AIPerceptionComponent->OnTargetPerceptionUpdated.AddDynamic(this, &AAllyTeamAIController::OnActorDetected);
}

ETeamAttitude::Type AAllyTeamAIController::GetTeamAttitudeTowards(const AActor &Other) const
{
    if (const APawn *OtherPawn = Cast<APawn>(&Other))
    {

        if (const IGenericTeamAgentInterface *TeamAgent = Cast<IGenericTeamAgentInterface>(OtherPawn->GetController()))
        {

            if (TeamAgent->GetGenericTeamId() == 0)
            {
                return ETeamAttitude::Friendly;
            }
            else
            {
                return ETeamAttitude::Hostile;
            }
        }
    }
    return ETeamAttitude::Hostile;
}

void AAllyTeamAIController::OnActorDetected(AActor *Actor, FAIStimulus Stimulus)
{
    PerceivedPawn = Cast<APawn>(Actor);

    OtherPawnInterface = Cast<IGenericTeamAgentInterface>(PerceivedPawn->GetController());

    FGenericTeamId OtherTeamID = OtherPawnInterface->GetGenericTeamId();

    if (OtherTeamID.GetId() == 1)
    {
        if (HasSeenHostile == false &&  SeenHostileActor!=nullptr)
        {
            HasSeenHostile = true;
            AllyBBD->SetValueAsBool(FName("HasSeenHostile?"), true);
            SeenHostileActor = Actor;
        }
    }
}

void AAllyTeamAIController::UnSetHostileSeen()
{
    HasSeenHostile = false;
    SeenHostileActor = nullptr;
    AllyBBD->SetValueAsBool(FName("HasSeenHostile?"), HasSeenHostile);
    UE_LOG(LogTemp, Warning, TEXT("Unsetting hostile"));
}

AActor *AAllyTeamAIController::GetHostilePawn_Implementation()
{
    return SeenHostileActor;
}

void AAllyTeamAIController::SetCanMove(bool CanMove)
{
    AllyBBD->SetValueAsBool(FName("CanMove?"), CanMove);
}

