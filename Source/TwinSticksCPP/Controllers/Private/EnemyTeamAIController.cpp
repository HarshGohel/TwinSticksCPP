// Fill out your copyright notice in the Description page of Project Settings.

#include "../Source/TwinSticksCPP/Controllers/Public/EnemyTeamAIController.h"
#include "BehaviorTree/BlackboardComponent.h" 

AEnemyTeamAIController::AEnemyTeamAIController()
{
    PrimaryActorTick.bCanEverTick = true;
    SetGenericTeamId(FGenericTeamId(1));

    AIPerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception"));
    this->AddOwnedComponent(AIPerceptionComponent);


    EnemyBBD = CreateDefaultSubobject<UBlackboardComponent>(TEXT("blackboard"));
    this->AddOwnedComponent(AIPerceptionComponent);
}

void AEnemyTeamAIController::BeginPlay()
{
    Super::BeginPlay();

    // UE_LOG(LogTemp, Warning, TEXT("BeginPlauy"));
    HasSeenHostile = false;

    // AIPerceptionComponent->OnPerceptionUpdated.AddDynamic(this, &AEnemyTeamAIController::OnPlayerDetected);
    AIPerceptionComponent->OnTargetPerceptionUpdated.AddDynamic(this, &AEnemyTeamAIController::OnActorDetected);

}

void AEnemyTeamAIController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

ETeamAttitude::Type AEnemyTeamAIController::GetTeamAttitudeTowards(const AActor &Other) const
{
    if (const APawn *OtherPawn = Cast<APawn>(&Other))
    {
        if (const IGenericTeamAgentInterface *TeamAgent = Cast<IGenericTeamAgentInterface>(OtherPawn->GetController()))
        {
            if (TeamAgent->GetGenericTeamId() == 1)
            {
                return ETeamAttitude::Friendly;
            }
            else
            {
                return ETeamAttitude::Hostile;
            }
        }
    }
    return ETeamAttitude::Hostile;
}

void AEnemyTeamAIController::OnActorDetected(AActor *Actor, FAIStimulus Stimulus)
{
    // // UE_LOG(LogTemp, Warning, TEXT("%s") , *Actor->GetActorNameOrLabel());

    PerceivedPawn = Cast<APawn>(Actor);

    OtherPawnInterface = Cast<IGenericTeamAgentInterface>(PerceivedPawn->GetController());

    FGenericTeamId OtherTeamID = OtherPawnInterface->GetGenericTeamId();

    if (OtherTeamID.GetId() == 0)
    {
        if (HasSeenHostile == false)
        {
            HasSeenHostile = true;
            EnemyBBD->SetValueAsBool(FName("HasSeenHostile?") , HasSeenHostile);
            
            SeenHostileActor = Actor;
        }
    }
}

void AEnemyTeamAIController::UnSetHostileSeen()
{
   
    HasSeenHostile = false;
    SeenHostileActor = nullptr;
    // Execute_BPUnSetHostileSeen(this);
    EnemyBBD->SetValueAsBool(FName("HasSeenHostile?") , HasSeenHostile);
    // UE_LOG(LogTemp, Warning, TEXT("Unsetting hostile"));
    // bool bisseen = EnemyBBD->GetValueAsBool(FName("HasSeenHostile?"));
    // if(bisseen)
    // {
    //     UE_LOG(LogTemp, Warning, TEXT("true"));
    // }
    // else
    // {
    //     UE_LOG(LogTemp, Warning, TEXT("false"));
    // }
}

AActor* AEnemyTeamAIController::GetHostilePawn_Implementation()
{
    return SeenHostileActor;
}

void AEnemyTeamAIController::SetCanMove(bool CanMove)
{
    EnemyBBD->SetValueAsBool("CanMove?" , CanMove);
}



// void AEnemyTeamAIController::OnPlayerDetected(HasSeenPlayer TArray<AActor *> &UpdatedActors)
// {
//     // UE_LOG(LogTemp, Warning, TEXT("%s"), *HostileActors[i]->GetActorNameOrLabel());
//     // UE_LOG(LogTemp, Warning, TEXT("no"));
//     AIPerceptionComponent->GetHostileActors(HostileActors);
//     for (auto &&i : HostileActors)
//     {
//         UE_LOG(LogTemp, Warning, TEXT("%s"), *i->GetActorNameOrLabel());
//     }
// }
