// Fill out your copyright notice in the Description page of Project Settings.


#include "../Source/TwinSticksCPP/Controllers/Public/MyPlayerController.h"

FGenericTeamId AMyPlayerController::GetGenericTeamId() const
{
    return FGenericTeamId(TeamId);
}

ETeamAttitude::Type AMyPlayerController::GetTeamAttitudeTowards(const AActor &Other) const
{
    if (const APawn *OtherPawn = Cast<APawn>(&Other))
    {

        if (const IGenericTeamAgentInterface *TeamAgent = Cast<IGenericTeamAgentInterface>(OtherPawn->GetController()))
        {

            if (TeamAgent->GetGenericTeamId() == 0)
            {
                return ETeamAttitude::Friendly;
            }
            else
            {
                return ETeamAttitude::Hostile;
            }
        }
    }
    return ETeamAttitude::Hostile;
}
