// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "GenericTeamAgentInterface.h"
#include "Perception/AIPerceptionComponent.h"
#include "../Source/TwinSticksCPP/Interfaces/Public/ShooterInterface.h"
#include "EnemyTeamAIController.generated.h"

/**
 *
 */

UCLASS()
class TWINSTICKSCPP_API AEnemyTeamAIController : public AAIController, public IShooterInterface
{
	GENERATED_BODY()

	AEnemyTeamAIController();

public:
	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditDefaultsOnly, Category = Components, BlueprintReadOnly)
	UAIPerceptionComponent *AIPerceptionComponent;

public:
	ETeamAttitude::Type GetTeamAttitudeTowards(const AActor &Other) const override;

private:
	bool HasSeenHostile = false;
	AActor* SeenHostileActor;

private:
	// UFUNCTION() void OnPlayerDetected(const TArray<AActor *> &UpdatedActors);
	UFUNCTION()
	void OnActorDetected(AActor *Actor, FAIStimulus Stimulus);

	// team
	const APawn *PerceivedPawn;
	const IGenericTeamAgentInterface *OtherPawnInterface;

public:
	// interface function
	void UnSetHostileSeen() override;
	AActor* GetHostilePawn_Implementation();
	

	public:
	void SetCanMove(bool CanMove) override;


	private:
	UPROPERTY(EditDefaultsOnly)
	class UBlackboardComponent *EnemyBBD;


	private:
	IShooterInterface *ShooterInterface;
};
