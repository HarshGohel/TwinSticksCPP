// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GenericTeamAgentInterface.h"
#include "../Source/TwinSticksCPP/Interfaces/Public/ShooterInterface.h"
#include "MyPlayerController.generated.h"

/**
 *
 */
UCLASS()
class TWINSTICKSCPP_API AMyPlayerController : public APlayerController, public IGenericTeamAgentInterface, public IShooterInterface
{
	GENERATED_BODY()

public:
	FGenericTeamId TeamId = FGenericTeamId(0);
	virtual FGenericTeamId GetGenericTeamId() const override;

public:
	ETeamAttitude::Type GetTeamAttitudeTowards(const AActor &Other) const override;


};
