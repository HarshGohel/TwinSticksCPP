// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "GenericTeamAgentInterface.h"
#include "../Source/TwinSticksCPP/Interfaces/Public/ShooterInterface.h"
#include "Perception/AIPerceptionComponent.h"
#include "AllyTeamAIController.generated.h"

/**
 * 
 */
UCLASS()
class TWINSTICKSCPP_API AAllyTeamAIController : public AAIController , public IShooterInterface
{
	GENERATED_BODY()

	AAllyTeamAIController();

	protected:
	virtual void BeginPlay() override;

	protected:
	UPROPERTY(EditDefaultsOnly , Category = Components , BlueprintReadOnly)
	UAIPerceptionComponent *AIPerceptionComponent;

	public:
  	ETeamAttitude::Type GetTeamAttitudeTowards(const AActor& Other) const override;

	//blackboard
	private:
	UPROPERTY(EditDefaultsOnly)
	class UBlackboardComponent *AllyBBD;
	bool HasSeenHostile = false;
	AActor* SeenHostileActor;
	

	//delagate
	private:
	UFUNCTION()
	void OnActorDetected(AActor *Actor, FAIStimulus Stimulus);


	
	// team
	const APawn *PerceivedPawn;
	const IGenericTeamAgentInterface *OtherPawnInterface;


	public:
	// interface function
	void UnSetHostileSeen() override;
	AActor* GetHostilePawn_Implementation();


	public:
	void SetCanMove(bool CanMove) override;
};
