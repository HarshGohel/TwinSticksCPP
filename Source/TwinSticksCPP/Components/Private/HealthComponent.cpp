// Fill out your copyright notice in the Description page of Project Settings.

#include "../Source/TwinSticksCPP/Components/Public/HealthComponent.h"
#include "../Source/TwinSticksCPP/Interfaces/Public/ShooterInterface.h"
#include "../Source/TwinSticksCPP/Gamemode/Public/CustomGameMode.h"
#include "../Source/TwinSticksCPP/Interfaces/Public/GamemodeInterface.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	Health = MaxHealth;
	GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::OnTakeDamage);
}

// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHealthComponent::ResetHealth()
{
	// UE_LOG(LogTemp, Warning, TEXT("reset health"));
	Health = MaxHealth;
}

void UHealthComponent::OnTakeDamage(AActor *DamagedActor, float Damage, const UDamageType *DamageType, AController *InstigatedBy, AActor *DamageCauser)
{
	// UE_LOG(LogTemp, Warning, TEXT("%s") , *DamagedActor->GetOwner()->GetName());

	I1 = Cast<IGenericTeamAgentInterface>(Cast<APawn>(DamagedActor)->GetController());
	// IGenericTeamAgentInterface *Test2 = Cast<IGenericTeamAgentInterface>(Cast<APawn>(DamageCauser));

	if (I1->GetTeamAttitudeTowards(*DamageCauser) == ETeamAttitude::Hostile)
	{
		// UE_LOG(LogTemp, Warning, TEXT("hostile"));
		Health -= Damage;
		if (Health <= 0)
		{
			// Call Handle Dealth On Damagedactor pawn

			Interface = Cast<IShooterInterface>(DamagedActor);
			IGamemodeInterface *Interface2 =  Cast<IGamemodeInterface>(Cast<ACustomGameMode>(UGameplayStatics::GetGameMode(this)));
			if (Interface)
			{
				Interface->HandleDeath();
				Interface2->PawnHandleDeath(DamagedActor);
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("invalid gamemode in healthcomponent"));
			}

			Interfacekill = Cast<IShooterInterface>(DamageCauser);
			if (Interfacekill)
			{
				Interfacekill->IncreaseKill();
			}
		}
	}
}

// GEngine->AddOnScreenDebugMessage(0, 1, FColor::Red, FString::Printf(TEXT("Health %f"), Health));

float UHealthComponent::GetHealth()
{
	return Health;
}
